#pragma once

#if defined(__APPLE__) && defined(__MACH__)
    #include "HIDDevice_osx.h"
#elif defined(_WIN32)
    #include "HIDDevice_win.h"
#endif
