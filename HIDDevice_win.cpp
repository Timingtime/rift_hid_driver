#include "pch.h"
#include "HIDDevice_win.h"

bool HIDDevice::init()
{
    m_hDeviceFile = findRiftDevice();
    if (m_hDeviceFile == INVALID_HANDLE_VALUE)
        return false;
    readDeviceStrings();
    readAttributes();
    return true;
}

void HIDDevice::close()
{
    if (m_hDeviceFile != INVALID_HANDLE_VALUE)
        CloseHandle(m_hDeviceFile);
    m_hDeviceFile = INVALID_HANDLE_VALUE;
}

void HIDDevice::setFeature(HIDMessage& feat) const
{
    feat.pack();
    HidD_SetFeature(m_hDeviceFile, 
        const_cast<HIDMessage*>(&feat)->getBuffer(), 
        feat.getBufferSize());
}

void HIDDevice::getFeature(HIDMessage& feat) const
{
    HidD_GetFeature(m_hDeviceFile, feat.getBuffer(), feat.getBufferSize());
    feat.unpack();
}

void HIDDevice::readDeviceStrings()
{
    wchar_t wstrBuffer[64];

    wstrBuffer[0] = 0;
    HidD_GetManufacturerString(m_hDeviceFile, wstrBuffer, 64);
    wcstombs_s(0, m_vendorName, sizeof(m_vendorName), wstrBuffer, 64);

    wstrBuffer[0] = 0;
    HidD_GetProductString(m_hDeviceFile, wstrBuffer, 64);
    wcstombs_s(0, m_productName, sizeof(m_productName), wstrBuffer, 64);

    wstrBuffer[0] = 0;
    HidD_GetSerialNumberString(m_hDeviceFile, wstrBuffer, 64);
    wcstombs_s(0, m_serialNumber, sizeof(m_serialNumber), wstrBuffer, 64);
}

void HIDDevice::readAttributes()
{
    HIDD_ATTRIBUTES attr = { sizeof(attr) };
    HidD_GetAttributes(m_hDeviceFile, &attr);
    m_product = attr.ProductID;
    m_vendor = attr.VendorID;
    m_version = attr.VersionNumber;
}

HANDLE HIDDevice::findRiftDevice()
{
    HANDLE hDeviceFile = INVALID_HANDLE_VALUE;

    // Get the GUID for the HID devices class
    GUID guid;
    HidD_GetHidGuid(&guid);

    // Get informations about a pool of devices.
    // DIGCF_PRESENT: get only the currently available devices
    // DIGCF_INTERFACEDEVICE: get only the devices that have an
    //   interface for the specified class GUID
    HDEVINFO devInfo = SetupDiGetClassDevs(&guid, NULL, NULL,
        DIGCF_PRESENT | DIGCF_INTERFACEDEVICE);

    // Device iterator
    SP_INTERFACE_DEVICE_DATA ifaceData = { sizeof(ifaceData) };

    for (int devID = 0;
        SetupDiEnumDeviceInterfaces(devInfo, NULL, &guid, devID, &ifaceData);
        devID++)
    {
        // Allocate a temporary storage and to store the device path
        // Let's use the stack instead of allocating the exact memory amount
        char data[128] = { sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA) };

        // Get the path of the device and put it in the temporary data storage
        SetupDiGetDeviceInterfaceDetail(devInfo, &ifaceData,
            (PSP_DEVICE_INTERFACE_DETAIL_DATA)data, 128, NULL, NULL);

        // Try to match this the device path with the string vid_2833&pid_0001 
        // which contains the vendorID 0x2238 and productID 0x01 of the Oculus Rift
        if (strstr(data + sizeof(DWORD), "vid_2833&pid_0001") != 0)
        {
            // Try to open the current device
            hDeviceFile = CreateFile(data + sizeof(DWORD),
                GENERIC_READ, FILE_SHARE_READ, NULL,
                OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

            // Stop if we found a match
            if (hDeviceFile != INVALID_HANDLE_VALUE)
                break;
        }
    }

    SetupDiDestroyDeviceInfoList(devInfo);

    return hDeviceFile;
}

unsigned long HIDDevice::read(HIDMessage& sens) const
{
    DWORD readBytes;
    ReadFile(m_hDeviceFile, sens.getBuffer(), sens.getBufferSize(), &readBytes, NULL);
    return readBytes;
}
