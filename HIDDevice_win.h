#pragma once

#include "pch.h"
#include "HIDMessage.h"

class HIDDevice
{
    HANDLE     m_hDeviceFile;
    char       m_productName[64];
    char       m_vendorName[64];
    char       m_serialNumber[64];
    int        m_vendor;
    int        m_product;
    int        m_version;

    HANDLE  findRiftDevice();
    void    readDeviceStrings();
    void    readAttributes();

public:
    HIDDevice() : m_hDeviceFile(INVALID_HANDLE_VALUE){}
    ~HIDDevice() { close(); }

    bool init();
    unsigned long read(HIDMessage& sens) const;
    void close();
    void getFeature(HIDMessage& feat) const;
    void setFeature(HIDMessage& feat) const;
    const char* getProductName() const { return m_productName; }
    const char* getVendorName() const { return m_vendorName; }
    const char* getSerialString() const { return m_serialNumber; }
    int getVendorID() const { return m_vendor; }
    int getProductID() const { return m_product; }
    int getVersionNumber() const { return m_version; }
};
