#include "cbuffer.h"

extern void drawSignal(const cbuffer<glm::vec3>& v, const float glm::vec3::* m, const glm::vec3& color);
extern void drawCube();
extern void drawVector(const glm::vec3& v, const glm::vec3& color);
extern void render();