#if defined(__APPLE__) && defined(__MACH__)
    #include <IOKit/hid/IOHIDManager.h>
    #define MSGBOX(H,M) CFUserNotificationDisplayAlert(\
        0, kCFUserNotificationCautionAlertLevel,\
        NULL, NULL, NULL, CFSTR(H), \
        CFSTR(M), NULL, NULL, NULL, NULL);
#elif defined(_WIN32)
    #define _CRT_SECURE_NO_WARNINGS
    #include <Windows.h>
    #include <hidsdi.h>
    #include <SetupAPI.h>
    #define MSGBOX(H,M) MessageBox(\
        NULL, M, H, MB_ICONEXCLAMATION);
#endif

#include <thread>
#include <queue>
#include <iostream>
#include <algorithm>
#include <thread>
#include <map>
#include <GLFW/glfw3.h>
#include "glm/glm/glm.hpp"
#include "glm/glm/gtc/matrix_transform.hpp"
#include "glm/glm/gtc/type_ptr.hpp"
#include "glm/glm/gtc/quaternion.hpp"
#include "glm/glm/gtx/vector_angle.hpp"
#include "glm/glm/gtx/euler_angles.hpp"
