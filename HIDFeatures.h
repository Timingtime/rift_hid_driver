#pragma once

#include "HIDMessage.h"

class HIDFeatureDisplayInfo : public HIDMessage
{
    unsigned char buffer[56];
public:
    unsigned short commandID;
    unsigned char  distortionType;
    unsigned short resolution[2];
    float screenSize[2];
    float vcenter;
    float lensSeparation;
    float eyeScreenDist[2];
    float distortionK[6];

    HIDFeatureDisplayInfo()
    {
        buffer[0] = 9;
        commandID = 0;
        distortionType = 0;
        resolution[0] = 0;
        resolution[1] = 0;
        screenSize[0] = 0;
        screenSize[1] = 0;
        vcenter = 0;
        lensSeparation = 0;
        eyeScreenDist[0] = 0.f;
        eyeScreenDist[1] = 0.f;
        for (int i = 0; i < 6; i++)
            distortionK[i] = 0.f;
        pack();
    }
    
    virtual HIDMessage& operator=(const HIDMessage& m) override
    {
        const HIDFeatureDisplayInfo& di = dynamic_cast<const HIDFeatureDisplayInfo&>(m);
        commandID = di.commandID;
        distortionType = di.distortionType;
        resolution[0] = di.resolution[0];
        resolution[1] = di.resolution[1];
        screenSize[0] = di.screenSize[0];
        screenSize[1] = di.screenSize[1];
        vcenter = di.vcenter;
        lensSeparation = di.lensSeparation;
        eyeScreenDist[0] = di.eyeScreenDist[0];
        eyeScreenDist[1] = di.eyeScreenDist[1];
        for (int i = 0; i < 6; i++)
            distortionK[i] = di.distortionK[i];
        std::memcpy(buffer, di.buffer, sizeof(buffer));
        return *this;
    }

    virtual void pack() override
    {
        buffer[0] = 9;
        packU16(commandID, buffer + 1);
        buffer[3] = distortionType;
        packU16(resolution[0], buffer + 4);
        packU16(resolution[1], buffer + 6);
        packU32(static_cast<unsigned long>(screenSize[0]    * 1000000.f), buffer + 8);
        packU32(static_cast<unsigned long>(screenSize[1]    * 1000000.f), buffer + 12);
        packU32(static_cast<unsigned long>(vcenter          * 1000000.f), buffer + 16);
        packU32(static_cast<unsigned long>(lensSeparation   * 1000000.f), buffer + 20);
        packU32(static_cast<unsigned long>(eyeScreenDist[0] * 1000000.f), buffer + 24);
        packU32(static_cast<unsigned long>(eyeScreenDist[1] * 1000000.f), buffer + 28);
        for (int i = 0; i < 6; i++)
            packF32(distortionK[i], buffer + 32 + i * 4);
    }
    virtual void unpack() override
    {
        commandID = unpackU16(buffer + 1);
        distortionType = buffer[3];
        resolution[0] = unpackU16(buffer + 4);
        resolution[1] = unpackU16(buffer + 6);
        screenSize[0] = unpackU32(buffer + 8)  * (1 / 1000000.f);
        screenSize[1] = unpackU32(buffer + 12) * (1 / 1000000.f);
        vcenter = unpackU32(buffer + 16) * (1 / 1000000.f);
        lensSeparation = unpackU32(buffer + 20) * (1 / 1000000.f);
        eyeScreenDist[0] = unpackU32(buffer + 24) * (1 / 1000000.f);
        eyeScreenDist[1] = unpackU32(buffer + 28) * (1 / 1000000.f);
        for (int i = 0; i < 6; i++)
            distortionK[i] = unpackF32(buffer + 32 + i * 4);
    }
    virtual unsigned char* getBuffer() override
    {
        return buffer;
    }
    virtual int getBufferSize() const override
    {
        return sizeof(buffer);
    }
};

class HIDFeatureKeepAlive : public HIDMessage
{
    unsigned char buffer[5];
public:
    unsigned short commandID;
    unsigned short keepAliveInterval;

    HIDFeatureKeepAlive()
    {
        buffer[0] = 8;
        commandID = 0;
        keepAliveInterval = 0;
        pack();
    }
    HIDFeatureKeepAlive(unsigned short ms)
    {
        buffer[0] = 8;
        commandID = 0;
        keepAliveInterval = ms;
        pack();
    }
    
    virtual HIDMessage& operator=(const HIDMessage& m) override
    {
        const HIDFeatureKeepAlive& ka = dynamic_cast<const HIDFeatureKeepAlive&>(m);
        commandID = ka.commandID;
        keepAliveInterval = ka.keepAliveInterval;
        std::memcpy(buffer, ka.buffer, sizeof(buffer));
        return *this;
    }

    virtual void pack() override
    {
        buffer[0] = 8;
        packU16(commandID, buffer + 1);
        packU16(keepAliveInterval, buffer + 3);
    }
    virtual void unpack() override
    {
        commandID = unpackU16(buffer + 1);
        keepAliveInterval = unpackU16(buffer + 3);
    }
    virtual unsigned char* getBuffer() override
    {
        return buffer;
    }
    virtual int getBufferSize() const override
    {
        return sizeof(buffer);
    }
};

class HIDFeatureSensorRange : public HIDMessage
{
    unsigned char buffer[8];
public:
    // In G m/s2 [2, 4, 8, 16]. Default max 4G. Up to 8G is safe.
    enum class AccelerationRange
    { 
        Default = 4,
        G_2     = 2, 
        G_4     = 4, 
        G_8     = 8, 
        G_16    = 16 
    };
    
    // In degrees [250, 500, 1000, 2000]. Default 2000. Up to 2000 is safe.
    enum class RotationRange
    { 
        Default      = 2000,
        Degrees_250  = 250,
        Degrees_500  = 500,
        Degrees_1000 = 1000,
        Degrees_2000 = 2000
    };
    // In Gauss * 0.001 [880, 1300, 1900, 2500]. Default 1.3 Gauss. Up to 2.5 Gauss is safe.
    enum class MagneticRange
    { 
        Default    = 1300,
        Gauss_0_88 = 880,
        Gauss_1_3  = 1300,
        Gauss_1_9  = 1900,
        Gauss_2_5  = 2500
    };

    unsigned short commandID;
    AccelerationRange acc;
    RotationRange gyr;
    MagneticRange mag;

    HIDFeatureSensorRange()
    {
        buffer[0] = 4;
        commandID = 0;
        acc = AccelerationRange::G_4;
        gyr = RotationRange::Degrees_2000;
        mag = MagneticRange::Gauss_2_5;
        pack();
    }
    HIDFeatureSensorRange(AccelerationRange ar, RotationRange rr, MagneticRange mr)
    {
        buffer[0] = 4;
        commandID = 0;
        acc = ar;
        gyr = rr;
        mag = mr;
        pack();
    }

    virtual HIDMessage& operator=(const HIDMessage& m) override
    {
        const HIDFeatureSensorRange& sr = dynamic_cast<const HIDFeatureSensorRange&>(m);
        commandID = sr.commandID;
        acc = sr.acc;
        gyr = sr.gyr;
        mag = sr.mag;
        std::memcpy(buffer, sr.buffer, sizeof(buffer));
        return *this;
    }

    virtual void pack() override
    {
        buffer[0] = 4;
        packU16(commandID, buffer + 1);
        buffer[3] = static_cast<unsigned char>(acc);
        packU16(static_cast<unsigned short>(gyr), buffer + 4);
        packU16(static_cast<unsigned short>(mag), buffer + 6);
    }
    virtual void unpack() override
    {
        commandID = unpackU16(buffer + 1);
        acc = static_cast<AccelerationRange>(buffer[3]);
        gyr = static_cast<RotationRange>(unpackU16(buffer + 4));
        mag = static_cast<MagneticRange>(unpackU16(buffer + 6));
    }
    virtual unsigned char* getBuffer() override
    {
        return buffer;
    }
    virtual int getBufferSize() const override
    {
        return sizeof(buffer);
    }
};
