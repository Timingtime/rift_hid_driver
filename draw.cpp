#include "pch.h"
#include "draw.h"

void drawSignal(const cbuffer<glm::vec3>& v, const float glm::vec3::* m, const glm::vec3& color)
{
    int x = 0;
    float hratio = 2.f / (float)v.m_count;
    glBegin(GL_LINE_STRIP);
    glColor3f(color.r, color.g, color.b);
    for (int i = v.m_index; i < v.m_count; i++)
        glVertex3f(hratio * x++ - 1.f, v.m_vec[i].*m / 15.f, 0.f);
    for (int i = 0; i < v.m_index; i++)
        glVertex3f(hratio * x++ - 1.f, v.m_vec[i].*m / 15.f, 0.f);
    glEnd();
}

void drawVector(const glm::vec3& v, const glm::vec3& color)
{
    glBegin(GL_LINES);
    glColor3f(color.r, color.g, color.b);

//    glLineWidth(5.0f);
    glVertex3f(0, 0, 0);
    glVertex3f(v.x, v.y, v.z);
    
//    glLineWidth(1.0f);
//    
//    glVertex3f(0, 0, 0);
//    glVertex3f(v.x, 0, 0);
//    
//    glVertex3f(0, 0, 0);
//    glVertex3f(0, v.y, 0);
//    
//    glVertex3f(0, 0, 0);
//    glVertex3f(0, 0, v.z);
    
    glEnd();
}

void drawCube()
{
    glBegin(GL_QUADS);
    glColor3f(0.0f, 1.0f, 0.0f);
    glVertex3f(1.0f, 1.0f, -1.0f);
    glVertex3f(-1.0f, 1.0f, -1.0f);
    glVertex3f(-1.0f, 1.0f, 1.0f);
    glVertex3f(1.0f, 1.0f, 1.0f);
    
    glColor3f(1.0f, 0.5f, 0.0f);
    glVertex3f(1.0f, -1.0f, 1.0f);
    glVertex3f(-1.0f, -1.0f, 1.0f);
    glVertex3f(-1.0f, -1.0f, -1.0f);
    glVertex3f(1.0f, -1.0f, -1.0f);
    
    glColor3f(1.0f, 0.0f, 0.0f);
    glVertex3f(1.0f, 1.0f, 1.0f);
    glVertex3f(-1.0f, 1.0f, 1.0f);
    glVertex3f(-1.0f, -1.0f, 1.0f);
    glVertex3f(1.0f, -1.0f, 1.0f);
    
    glColor3f(1.0f, 1.0f, 0.0f);
    glVertex3f(1.0f, -1.0f, -1.0f);
    glVertex3f(-1.0f, -1.0f, -1.0f);
    glVertex3f(-1.0f, 1.0f, -1.0f);
    glVertex3f(1.0f, 1.0f, -1.0f);
    
    glColor3f(0.0f, 0.0f, 1.0f);
    glVertex3f(-1.0f, 1.0f, 1.0f);
    glVertex3f(-1.0f, 1.0f, -1.0f);
    glVertex3f(-1.0f, -1.0f, -1.0f);
    glVertex3f(-1.0f, -1.0f, 1.0f);
    
    glColor3f(1.0f, 0.0f, 1.0f);
    glVertex3f(1.0f, 1.0f, -1.0f);
    glVertex3f(1.0f, 1.0f, 1.0f);
    glVertex3f(1.0f, -1.0f, 1.0f);
    glVertex3f(1.0f, -1.0f, -1.0f);
    glEnd();
}
